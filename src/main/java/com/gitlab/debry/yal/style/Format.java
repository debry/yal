package com.gitlab.debry.yal.style;

public class Format extends Style
{
    private String format;

    public Format(final String format)
    {
        super(E_Style.FORMAT);
        this.format = format;
    }

    public String getFormat()
    {
        return format;
    }

    @Override
    public Format clone()
    {
        return new Format(format);
    }
}
