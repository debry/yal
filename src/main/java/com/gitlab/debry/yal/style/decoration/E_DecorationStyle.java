package com.gitlab.debry.yal.style.decoration;

public enum E_DecorationStyle
{
    BLINK,
    UNDERLINE,
    STRIKETHROUGH
}
