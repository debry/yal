package com.gitlab.debry.yal.style.color;

public enum E_ColorStyle
{
    FOREGROUND,
    BACKGROUND
}
