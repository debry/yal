package com.gitlab.debry.yal.style.decoration;

public class DecorationBlink extends Decoration
{
    public DecorationBlink()
    {
        super(E_DecorationStyle.BLINK);
    }

    @Override
    public DecorationBlink clone()
    {
        return new DecorationBlink();
    }
}
