package com.gitlab.debry.yal.style.font;

public class FontWeight extends Font
{
    private E_FontWeight weight;

    public FontWeight(final E_FontWeight weight)
    {
        super(E_FontStyle.WEIGHT);
        this.weight = weight;
    }

    public E_FontWeight getFontWeight()
    {
        return weight;
    }

    @Override
    public FontWeight clone()
    {
        return new FontWeight(weight);
    }
}
