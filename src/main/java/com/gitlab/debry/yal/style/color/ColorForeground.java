package com.gitlab.debry.yal.style.color;

public class ColorForeground extends Color
{
    public ColorForeground(final String colorName)
    {
        this(E_ColorName.valueOf(colorName.toUpperCase()));
    }

    public ColorForeground(final E_ColorName colorName)
    {
        super(E_ColorStyle.FOREGROUND, colorName);
    }

    @Override
    public ColorForeground clone()
    {
        return new ColorForeground(colorName);
    }
}
