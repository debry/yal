package com.gitlab.debry.yal.style;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.gitlab.debry.yal.style.color.Color;
import com.gitlab.debry.yal.style.color.ColorBackground;
import com.gitlab.debry.yal.style.color.ColorForeground;
import com.gitlab.debry.yal.style.decoration.Decoration;
import com.gitlab.debry.yal.style.decoration.DecorationBlink;
import com.gitlab.debry.yal.style.decoration.DecorationStrikeThrough;
import com.gitlab.debry.yal.style.decoration.DecorationUnderline;
import com.gitlab.debry.yal.style.decoration.E_DecorationStyle;
import com.gitlab.debry.yal.style.font.E_FontWeight;
import com.gitlab.debry.yal.style.font.Font;
import com.gitlab.debry.yal.style.font.FontFamily;
import com.gitlab.debry.yal.style.font.FontSize;
import com.gitlab.debry.yal.style.font.FontWeight;

public abstract class Style
{
    private final E_Style type;

    public Style(final E_Style type)
    {
        this.type = type;
    }

    public E_Style getType()
    {
        return type;
    }

    protected Style clone()
    {
        return null;
    }

    static public void deepCopy(final List<Style> originStyle, final List<Style> copyStyle)
    {
        for (final Style style : originStyle)
            copyStyle.add(style.clone());
    }

    static public void addReplace(final Style style, final List<Style> styles)
    {
        if (style != null)
        {
            for (final Style st : styles)
                if (st.getType() == style.getType())
                {
                    if (style.getType() == E_Style.FONT)
                    {
                        if (((Font) st).getFontStyle() == ((Font) style).getFontStyle())
                            styles.remove(st);
                    }
                    else if (style.getType() == E_Style.DECORATION)
                    {
                        if (((Decoration) st).getDecorationStyle() == ((Decoration) style).getDecorationStyle())
                            styles.remove(st);
                    }
                    else if (style.getType() == E_Style.COLOR)
                    {
                        if (((Color) st).getColorStyle() == ((Color) style).getColorStyle())
                            styles.remove(st);
                    }
                    else if (style.getType() == E_Style.FORMAT)
                        styles.remove(st);
                }

            styles.add(style.clone());
        }
    }

    static public List<Style> merge(final List<Style> defaultStyle, final List<Style> customStyle)
    {
        final List<Style> localStyle = new LinkedList<Style>();

        for (final Style style : defaultStyle)
            localStyle.add(style.clone());

        for (final Style style : customStyle)
            Style.addReplace(style, localStyle);

        return localStyle;
    }

    private static Style parse(final String key, final Object object)
    {
        final String[] keyArray = key.split("\\.");

        if (keyArray[0].equals("font"))
        {
            if (keyArray[1].equals("weight"))
                return new FontWeight(E_FontWeight.valueOf(((String) object).toUpperCase()));
            else if (keyArray[1].equals("size"))
                return new FontSize(((Integer) object).intValue());
            else if (keyArray[1].equals("family"))
                return new FontFamily(((String) object));
        }
        else if (keyArray[0].equals("color"))
        {
            if (keyArray[1].equals("background"))
                return new ColorBackground((String) object);
            else if (keyArray[1].equals("foreground"))
                return new ColorForeground((String) object);
        }
        else if (keyArray[0].equals("decoration"))
        {
            final E_DecorationStyle decorationStyle = E_DecorationStyle
                    .valueOf(((String) object).toUpperCase());
            switch (decorationStyle)
            {
                case BLINK:
                    return new DecorationBlink();
                case UNDERLINE:
                    return new DecorationUnderline();
                case STRIKETHROUGH:
                    return new DecorationStrikeThrough();
            }
        }
        else if (keyArray[0].equals("format"))
            return new Format((String) object);

        return null;
    }

    public static void fromMap(final Map<String, Object> styleMap, final List<Style> style)
    {
        if (!styleMap.isEmpty())
            for (final Map.Entry<String, Object> entry : styleMap.entrySet())
                style.add(parse(entry.getKey(), entry.getValue()));
    }
}
