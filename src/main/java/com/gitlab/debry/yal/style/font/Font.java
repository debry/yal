package com.gitlab.debry.yal.style.font;

import com.gitlab.debry.yal.style.Style;
import com.gitlab.debry.yal.style.E_Style;

public abstract class Font extends Style
{
    private E_FontStyle fontStyle;

    public Font(final E_FontStyle fontStyle)
    {
        super(E_Style.FONT);
        this.fontStyle = fontStyle;
    }

    public E_FontStyle getFontStyle()
    {
        return fontStyle;
    }
}
