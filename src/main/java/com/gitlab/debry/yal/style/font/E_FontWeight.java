package com.gitlab.debry.yal.style.font;

public enum E_FontWeight
{
    NORMAL,
    BOLD,
    ITALIC
}
