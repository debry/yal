package com.gitlab.debry.yal.style.decoration;

public class DecorationStrikeThrough extends Decoration
{
    public DecorationStrikeThrough()
    {
        super(E_DecorationStyle.STRIKETHROUGH);
    }

    @Override
    public DecorationStrikeThrough clone()
    {
        return new DecorationStrikeThrough();
    }
}
