package com.gitlab.debry.yal.style.font;

public class FontSize extends Font
{
    private int size;

    public FontSize(final int size)
    {
        super(E_FontStyle.SIZE);
        this.size = size;
    }

    public int getFontSize()
    {
        return size;
    }

    @Override
    public FontSize clone()
    {
        return new FontSize(size);
    }
}
