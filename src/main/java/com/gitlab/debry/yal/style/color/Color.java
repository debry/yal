package com.gitlab.debry.yal.style.color;

import java.util.HashMap;
import java.util.Map;

import com.gitlab.debry.yal.style.E_Style;
import com.gitlab.debry.yal.style.Style;

public abstract class Color extends Style
{
    static public Map<E_ColorName, int[]> nameToRGB = new HashMap<E_ColorName, int[]>()
    {
        private static final long serialVersionUID = 1L;

        {
            put(E_ColorName.ALICEBLUE, new int[] { 240, 248, 255 });
            put(E_ColorName.ANTIQUEWHITE, new int[] { 250, 235, 215 });
            put(E_ColorName.AQUA, new int[] { 0, 255, 255 });
            put(E_ColorName.AQUAMARINE, new int[] { 127, 255, 212 });
            put(E_ColorName.AZURE, new int[] { 240, 255, 255 });
            put(E_ColorName.BEIGE, new int[] { 245, 245, 220 });
            put(E_ColorName.BISQUE, new int[] { 255, 228, 196 });
            put(E_ColorName.BLACK, new int[] { 0, 0, 0 });
            put(E_ColorName.BLANCHEDALMOND, new int[] { 255, 235, 205 });
            put(E_ColorName.BLUE, new int[] { 0, 0, 255 });
            put(E_ColorName.BLUEVIOLET, new int[] { 138, 43, 226 });
            put(E_ColorName.BROWN, new int[] { 165, 42, 42 });
            put(E_ColorName.BURLYWOOD, new int[] { 222, 184, 135 });
            put(E_ColorName.CADETBLUE, new int[] { 95, 158, 160 });
            put(E_ColorName.CHARTREUSE, new int[] { 127, 255, 0 });
            put(E_ColorName.CHOCOLATE, new int[] { 210, 105, 30 });
            put(E_ColorName.CORAL, new int[] { 255, 127, 80 });
            put(E_ColorName.CORNFLOWERBLUE, new int[] { 100, 149, 237 });
            put(E_ColorName.CORNSILK, new int[] { 255, 248, 220 });
            put(E_ColorName.CRIMSON, new int[] { 220, 20, 60 });
            put(E_ColorName.CYAN, new int[] { 0, 255, 255 });
            put(E_ColorName.DARKBLUE, new int[] { 0, 0, 139 });
            put(E_ColorName.DARKCYAN, new int[] { 0, 139, 139 });
            put(E_ColorName.DARKGOLDENROD, new int[] { 184, 134, 11 });
            put(E_ColorName.DARKGRAY, new int[] { 169, 169, 169 });
            put(E_ColorName.DARKGREEN, new int[] { 0, 100, 0 });
            put(E_ColorName.DARKGREY, new int[] { 169, 169, 169 });
            put(E_ColorName.DARKKHAKI, new int[] { 189, 183, 107 });
            put(E_ColorName.DARKMAGENTA, new int[] { 139, 0, 139 });
            put(E_ColorName.DARKOLIVEGREEN, new int[] { 85, 107, 47 });
            put(E_ColorName.DARKORANGE, new int[] { 255, 140, 0 });
            put(E_ColorName.DARKORCHID, new int[] { 153, 50, 204 });
            put(E_ColorName.DARKRED, new int[] { 139, 0, 0 });
            put(E_ColorName.DARKSALMON, new int[] { 233, 150, 122 });
            put(E_ColorName.DARKSEAGREEN, new int[] { 143, 188, 143 });
            put(E_ColorName.DARKSLATEBLUE, new int[] { 72, 61, 139 });
            put(E_ColorName.DARKSLATEGRAY, new int[] { 47, 79, 79 });
            put(E_ColorName.DARKSLATEGREY, new int[] { 47, 79, 79 });
            put(E_ColorName.DARKTURQUOISE, new int[] { 0, 206, 209 });
            put(E_ColorName.DARKVIOLET, new int[] { 148, 0, 211 });
            put(E_ColorName.DEEPPINK, new int[] { 255, 20, 147 });
            put(E_ColorName.DEEPSKYBLUE, new int[] { 0, 191, 255 });
            put(E_ColorName.DIMGRAY, new int[] { 105, 105, 105 });
            put(E_ColorName.DIMGREY, new int[] { 105, 105, 105 });
            put(E_ColorName.DODGERBLUE, new int[] { 30, 144, 255 });
            put(E_ColorName.FIREBRICK, new int[] { 178, 34, 34 });
            put(E_ColorName.FLORALWHITE, new int[] { 255, 250, 240 });
            put(E_ColorName.FORESTGREEN, new int[] { 34, 139, 34 });
            put(E_ColorName.FUCHSIA, new int[] { 255, 0, 255 });
            put(E_ColorName.GAINSBORO, new int[] { 220, 220, 220 });
            put(E_ColorName.GHOSTWHITE, new int[] { 248, 248, 255 });
            put(E_ColorName.GOLD, new int[] { 255, 215, 0 });
            put(E_ColorName.GOLDENROD, new int[] { 218, 165, 32 });
            put(E_ColorName.GRAY, new int[] { 128, 128, 128 });
            put(E_ColorName.GREEN, new int[] { 0, 128, 0 });
            put(E_ColorName.GREENYELLOW, new int[] { 173, 255, 47 });
            put(E_ColorName.GREY, new int[] { 128, 128, 128 });
            put(E_ColorName.HONEYDEW, new int[] { 240, 255, 240 });
            put(E_ColorName.HOTPINK, new int[] { 255, 105, 180 });
            put(E_ColorName.INDIANRED, new int[] { 205, 92, 92 });
            put(E_ColorName.INDIGO, new int[] { 75, 0, 130 });
            put(E_ColorName.IVORY, new int[] { 255, 255, 240 });
            put(E_ColorName.KHAKI, new int[] { 240, 230, 140 });
            put(E_ColorName.LAVENDER, new int[] { 230, 230, 250 });
            put(E_ColorName.LAVENDERBLUSH, new int[] { 255, 240, 245 });
            put(E_ColorName.LAWNGREEN, new int[] { 124, 252, 0 });
            put(E_ColorName.LEMONCHIFFON, new int[] { 255, 250, 205 });
            put(E_ColorName.LIGHTBLUE, new int[] { 173, 216, 230 });
            put(E_ColorName.LIGHTCORAL, new int[] { 240, 128, 128 });
            put(E_ColorName.LIGHTCYAN, new int[] { 224, 255, 255 });
            put(E_ColorName.LIGHTGOLDENRODYELLOW, new int[] { 250, 250, 210 });
            put(E_ColorName.LIGHTGRAY, new int[] { 211, 211, 211 });
            put(E_ColorName.LIGHTGREEN, new int[] { 144, 238, 144 });
            put(E_ColorName.LIGHTGREY, new int[] { 211, 211, 211 });
            put(E_ColorName.LIGHTPINK, new int[] { 255, 182, 193 });
            put(E_ColorName.LIGHTSALMON, new int[] { 255, 160, 122 });
            put(E_ColorName.LIGHTSEAGREEN, new int[] { 32, 178, 170 });
            put(E_ColorName.LIGHTSKYBLUE, new int[] { 135, 206, 250 });
            put(E_ColorName.LIGHTSLATEGRAY, new int[] { 119, 136, 153 });
            put(E_ColorName.LIGHTSLATEGREY, new int[] { 119, 136, 153 });
            put(E_ColorName.LIGHTSTEELBLUE, new int[] { 176, 196, 222 });
            put(E_ColorName.LIGHTYELLOW, new int[] { 255, 255, 224 });
            put(E_ColorName.LIME, new int[] { 0, 255, 0 });
            put(E_ColorName.LIMEGREEN, new int[] { 50, 205, 50 });
            put(E_ColorName.LINEN, new int[] { 250, 240, 230 });
            put(E_ColorName.MAGENTA, new int[] { 255, 0, 255 });
            put(E_ColorName.MAROON, new int[] { 128, 0, 0 });
            put(E_ColorName.MEDIUMAQUAMARINE, new int[] { 102, 205, 170 });
            put(E_ColorName.MEDIUMBLUE, new int[] { 0, 0, 205 });
            put(E_ColorName.MEDIUMORCHID, new int[] { 186, 85, 211 });
            put(E_ColorName.MEDIUMPURPLE, new int[] { 147, 112, 219 });
            put(E_ColorName.MEDIUMSEAGREEN, new int[] { 60, 179, 113 });
            put(E_ColorName.MEDIUMSLATEBLUE, new int[] { 123, 104, 238 });
            put(E_ColorName.MEDIUMSPRINGGREEN, new int[] { 0, 250, 154 });
            put(E_ColorName.MEDIUMTURQUOISE, new int[] { 72, 209, 204 });
            put(E_ColorName.MEDIUMVIOLETRED, new int[] { 199, 21, 133 });
            put(E_ColorName.MIDNIGHTBLUE, new int[] { 25, 25, 112 });
            put(E_ColorName.MINTCREAM, new int[] { 245, 255, 250 });
            put(E_ColorName.MISTYROSE, new int[] { 255, 228, 225 });
            put(E_ColorName.MOCCASIN, new int[] { 255, 228, 181 });
            put(E_ColorName.NAVAJOWHITE, new int[] { 255, 222, 173 });
            put(E_ColorName.NAVY, new int[] { 0, 0, 128 });
            put(E_ColorName.OLDLACE, new int[] { 253, 245, 230 });
            put(E_ColorName.OLIVE, new int[] { 128, 128, 0 });
            put(E_ColorName.OLIVEDRAB, new int[] { 107, 142, 35 });
            put(E_ColorName.ORANGE, new int[] { 255, 165, 0 });
            put(E_ColorName.ORANGERED, new int[] { 255, 69, 0 });
            put(E_ColorName.ORCHID, new int[] { 218, 112, 214 });
            put(E_ColorName.PALEGOLDENROD, new int[] { 238, 232, 170 });
            put(E_ColorName.PALEGREEN, new int[] { 152, 251, 152 });
            put(E_ColorName.PALETURQUOISE, new int[] { 175, 238, 238 });
            put(E_ColorName.PALEVIOLETRED, new int[] { 219, 112, 147 });
            put(E_ColorName.PAPAYAWHIP, new int[] { 255, 239, 213 });
            put(E_ColorName.PEACHPUFF, new int[] { 255, 218, 185 });
            put(E_ColorName.PERU, new int[] { 205, 133, 63 });
            put(E_ColorName.PINK, new int[] { 255, 192, 203 });
            put(E_ColorName.PLUM, new int[] { 221, 160, 221 });
            put(E_ColorName.POWDERBLUE, new int[] { 176, 224, 230 });
            put(E_ColorName.PURPLE, new int[] { 128, 0, 128 });
            put(E_ColorName.RED, new int[] { 255, 0, 0 });
            put(E_ColorName.ROSYBROWN, new int[] { 188, 143, 143 });
            put(E_ColorName.ROYALBLUE, new int[] { 65, 105, 225 });
            put(E_ColorName.SADDLEBROWN, new int[] { 139, 69, 19 });
            put(E_ColorName.SALMON, new int[] { 250, 128, 114 });
            put(E_ColorName.SANDYBROWN, new int[] { 244, 164, 96 });
            put(E_ColorName.SEAGREEN, new int[] { 46, 139, 87 });
            put(E_ColorName.SEASHELL, new int[] { 255, 245, 238 });
            put(E_ColorName.SIENNA, new int[] { 160, 82, 45 });
            put(E_ColorName.SILVER, new int[] { 192, 192, 192 });
            put(E_ColorName.SKYBLUE, new int[] { 135, 206, 235 });
            put(E_ColorName.SLATEBLUE, new int[] { 106, 90, 205 });
            put(E_ColorName.SLATEGRAY, new int[] { 112, 128, 144 });
            put(E_ColorName.SLATEGREY, new int[] { 112, 128, 144 });
            put(E_ColorName.SNOW, new int[] { 255, 250, 250 });
            put(E_ColorName.SPRINGGREEN, new int[] { 0, 255, 127 });
            put(E_ColorName.STEELBLUE, new int[] { 70, 130, 180 });
            put(E_ColorName.TAN, new int[] { 210, 180, 140 });
            put(E_ColorName.TEAL, new int[] { 0, 128, 128 });
            put(E_ColorName.THISTLE, new int[] { 216, 191, 216 });
            put(E_ColorName.TOMATO, new int[] { 255, 99, 71 });
            put(E_ColorName.TURQUOISE, new int[] { 64, 224, 208 });
            put(E_ColorName.VIOLET, new int[] { 238, 130, 238 });
            put(E_ColorName.WHEAT, new int[] { 245, 222, 179 });
            put(E_ColorName.WHITE, new int[] { 255, 255, 255 });
            put(E_ColorName.WHITESMOKE, new int[] { 245, 245, 245 });
            put(E_ColorName.YELLOW, new int[] { 255, 255, 0 });
            put(E_ColorName.YELLOWGREEN, new int[] { 154, 205, 50 });
            put(E_ColorName.BASE03, new int[] { 0, 43, 54 });
            put(E_ColorName.BASE02, new int[] { 7, 54, 66 });
            put(E_ColorName.BASE01, new int[] { 88, 110, 117 });
            put(E_ColorName.BASE00, new int[] { 101, 123, 131 });
            put(E_ColorName.BASE0, new int[] { 131, 148, 150 });
            put(E_ColorName.BASE1, new int[] { 147, 161, 161 });
            put(E_ColorName.BASE2, new int[] { 238, 232, 213 });
            put(E_ColorName.BASE3, new int[] { 253, 246, 227 });
        }
    };

    private final E_ColorStyle colorStyle;

    protected E_ColorName colorName;

    public Color(final E_ColorStyle colorStyle, final E_ColorName colorName)
    {
        super(E_Style.COLOR);
        this.colorStyle = colorStyle;
        this.colorName = colorName;
    }

    public E_ColorStyle getColorStyle()
    {
        return colorStyle;
    }

    public E_ColorName getColorName()
    {
        return colorName;
    }
}
