package com.gitlab.debry.yal.style.font;

public enum E_FontStyle
{
    FAMILY,
    WEIGHT,
    SIZE
}
