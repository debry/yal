package com.gitlab.debry.yal.style.decoration;

public class DecorationUnderline extends Decoration
{
    public DecorationUnderline()
    {
        super(E_DecorationStyle.UNDERLINE);
    }

    @Override
    public DecorationUnderline clone()
    {
        return new DecorationUnderline();
    }
}
