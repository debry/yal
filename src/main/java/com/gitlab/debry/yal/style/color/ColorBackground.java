package com.gitlab.debry.yal.style.color;

public class ColorBackground extends Color
{
    public ColorBackground(final String colorName)
    {
        this(E_ColorName.valueOf(colorName.toUpperCase()));
    }

    public ColorBackground(final E_ColorName colorName)
    {
        super(E_ColorStyle.BACKGROUND, colorName);
    }

    @Override
    public ColorBackground clone()
    {
        return new ColorBackground(colorName);
    }
}
