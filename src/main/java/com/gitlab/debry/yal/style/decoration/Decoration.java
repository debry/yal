package com.gitlab.debry.yal.style.decoration;

import com.gitlab.debry.yal.style.Style;
import com.gitlab.debry.yal.style.E_Style;

public abstract class Decoration extends Style
{
    private E_DecorationStyle decorationStyle;

    public Decoration(final E_DecorationStyle decorationStyle)
    {
        super(E_Style.DECORATION);
        this.decorationStyle = decorationStyle;
    }

    public E_DecorationStyle getDecorationStyle()
    {
        return decorationStyle;
    }
}
