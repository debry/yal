package com.gitlab.debry.yal.style.font;

public class FontFamily extends Font
{
    private String name;

    public FontFamily(final String name)
    {
        super(E_FontStyle.FAMILY);
        this.name = name;
    }

    public String getFontName()
    {
        return name;
    }

    @Override
    public FontFamily clone()
    {
        return new FontFamily(name);
    }
}
