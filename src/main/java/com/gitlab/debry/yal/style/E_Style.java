package com.gitlab.debry.yal.style;

public enum E_Style
{
    FORMAT,
    COLOR,
    FONT,
    DECORATION
}
