package com.gitlab.debry.yal;

import java.util.LinkedList;
import java.util.List;

import com.gitlab.debry.yal.input.Bool;
import com.gitlab.debry.yal.input.E_Input;
import com.gitlab.debry.yal.input.Endl;
import com.gitlab.debry.yal.input.Input;
import com.gitlab.debry.yal.input.Int;
import com.gitlab.debry.yal.input.Obj;
import com.gitlab.debry.yal.input.Real;
import com.gitlab.debry.yal.input.Separator;
import com.gitlab.debry.yal.input.Space;
import com.gitlab.debry.yal.input.Str;
import com.gitlab.debry.yal.input.Time;
import com.gitlab.debry.yal.input.array.Matrix;
import com.gitlab.debry.yal.input.array.Vector;
import com.gitlab.debry.yal.input.level.Debug;
import com.gitlab.debry.yal.input.level.Fatal;
import com.gitlab.debry.yal.input.level.Info;
import com.gitlab.debry.yal.input.level.Level;
import com.gitlab.debry.yal.input.level.User;
import com.gitlab.debry.yal.input.level.Warn;
import com.gitlab.debry.yal.input.level.YalError;
import com.gitlab.debry.yal.input.stack.ClassName;
import com.gitlab.debry.yal.input.stack.FileName;
import com.gitlab.debry.yal.input.stack.LineNumber;
import com.gitlab.debry.yal.input.stack.MethodName;
import com.gitlab.debry.yal.input.stack.StackAll;
import com.gitlab.debry.yal.output.JConsole;
import com.gitlab.debry.yal.style.Format;
import com.gitlab.debry.yal.style.Style;
import com.gitlab.debry.yal.style.color.ColorBackground;
import com.gitlab.debry.yal.style.color.ColorForeground;
import com.gitlab.debry.yal.style.color.E_ColorName;
import com.gitlab.debry.yal.style.decoration.DecorationBlink;
import com.gitlab.debry.yal.style.decoration.DecorationStrikeThrough;
import com.gitlab.debry.yal.style.decoration.DecorationUnderline;
import com.gitlab.debry.yal.style.font.E_FontWeight;
import com.gitlab.debry.yal.style.font.FontFamily;
import com.gitlab.debry.yal.style.font.FontSize;
import com.gitlab.debry.yal.style.font.FontWeight;

public class YAL
{
    private Input input;

    private Input last;

    private final List<JConsole> jconsoles;

    private boolean noSeparator;

    public YAL()
    {
        input = null;
        last = null;
        jconsoles = new LinkedList<JConsole>();
        noSeparator = false;
    }

    private void send()
    {
        if (input != null && input.getType() == E_Input.LEVEL)
            for (final JConsole jconsole : jconsoles)
                if ((((Level) input).getLevel() & jconsole.getLevel()) > 0)
                    jconsole.receive(input);

        input = null;
        last = null;
    }

    public YAL addJConsole(final JConsole jconsole)
    {
        jconsoles.add(jconsole);
        return this;
    }

    public YAL Space()
    {
        return Space(1);
    }

    public YAL Space(final int n)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Space(n));
        return this;
    }

    public YAL newLine()
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Endl());
        return this;
    }

    public void Endl()
    {
        if (last != null)
            last = last.setNext(new Endl());
        send();
    }

    public YAL setLevel(final Level level)
    {
        input = level;
        last = input;
        return this;
    }

    public YAL DEBUG(final int subLevel)
    {
        return setLevel(new Debug(subLevel));
    }

    public YAL INFO(final int subLevel)
    {
        return setLevel(new Info(subLevel));
    }

    public YAL WARN(final int subLevel)
    {
        return setLevel(new Warn(subLevel));
    }

    public YAL ERROR()
    {
        return setLevel(new YalError());
    }

    public YAL FATAL()
    {
        return setLevel(new Fatal());
    }

    public YAL USER(final int subLevel)
    {
        return setLevel(new User(subLevel));
    }

    private void insertSeparator(final boolean yesSeparator)
    {
        if (last != null
                && ((last.getType() != E_Input.SEP &&
                        last.getType() != E_Input.ENDL &&
                        (last.getType() == E_Input.STACK ||
                                last.getType() == E_Input.TIME ||
                                last.getType() == E_Input.LEVEL)
                        && !noSeparator)
                        || yesSeparator))
            last = last.setNext(new Separator());
        noSeparator = false;
    }

    public YAL Sep(final String separator)
    {
        if (last != null && last.getType() != E_Input.SEP)
            last = last.setNext(new Separator(separator));
        return this;
    }

    public YAL Sep()
    {
        if (last != null && last.getType() != E_Input.SEP)
            last = last.setNext(new Separator());
        return this;
    }

    public YAL NoSep()
    {
        noSeparator = true;
        return this;
    }

    public YAL Time()
    {
        insertSeparator(true);
        if (last != null)
            last = last.setNext(new Time());
        return this;
    }

    public YAL Int(final int inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Int(inputValue));
        return this;
    }

    public YAL Real(final double inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Real(inputValue));
        return this;
    }

    public YAL Bool(final boolean inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Bool(inputValue));
        return this;
    }

    public YAL Str(final String inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Str(inputValue));
        return this;
    }

    public YAL Obj(final Object inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Obj(inputValue));
        return this;
    }

    public YAL Vector(final int[] inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Vector(inputValue));
        return this;
    }

    public YAL Vector(final double[] inputValue)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Vector(inputValue));
        return this;
    }

    public YAL Vector(final int[] inputValue, final int begin, final int end)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Vector(inputValue, begin, end));
        return this;
    }

    public YAL Vector(final double[] inputValue, final int begin, final int end)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Vector(inputValue, begin, end));
        return this;
    }

    public YAL Matrix(final int[] inputValue, final int rows, final int cols)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Matrix(inputValue, rows, cols));
        return this;
    }

    public YAL Matrix(final double[] inputValue, final int rows, final int cols)
    {
        insertSeparator(false);
        if (last != null)
            last = last.setNext(new Matrix(inputValue, rows, cols));
        return this;
    }

    public YAL lineNo()
    {
        insertSeparator(true);
        if (last != null)
            last = last.setNext(new LineNumber());
        return this;
    }

    public YAL methodName()
    {
        insertSeparator(true);
        if (last != null)
            last = last.setNext(new MethodName());
        return this;
    }

    public YAL fileName()
    {
        insertSeparator(true);
        if (last != null)
            last = last.setNext(new FileName());
        return this;
    }

    public YAL className()
    {
        insertSeparator(true);
        if (last != null)
            last = last.setNext(new ClassName());
        return this;
    }

    public YAL stackAll()
    {
        insertSeparator(true);
        if (last != null)
            last = last.setNext(new StackAll());
        return this;
    }

    public YAL addStyle(final Style style)
    {
        if (last != null)
            last.addStyle(style);
        return this;
    }

    public YAL format(final String format)
    {
        if (last != null)
            last.addStyle(new Format(format));
        return this;
    }

    public YAL fontFamily(final String fontName)
    {
        return addStyle(new FontFamily(fontName));
    }

    public YAL fontWeight(final E_FontWeight fontWeight)
    {
        return addStyle(new FontWeight(fontWeight));
    }

    public YAL bold()
    {
        return fontWeight(E_FontWeight.BOLD);
    }

    public YAL normal()
    {
        return fontWeight(E_FontWeight.NORMAL);
    }

    public YAL italic()
    {
        return fontWeight(E_FontWeight.ITALIC);
    }

    public YAL fontSize(final int fontSize)
    {
        return addStyle(new FontSize(fontSize));
    }

    public YAL underLine()
    {
        return addStyle(new DecorationUnderline());
    }

    public YAL blink()
    {
        return addStyle(new DecorationBlink());
    }

    public YAL strikeThrough()
    {
        return addStyle(new DecorationStrikeThrough());
    }

    public YAL fgColor(final E_ColorName colorName)
    {
        return addStyle(new ColorForeground(colorName));
    }

    public YAL bgColor(final E_ColorName colorName)
    {
        return addStyle(new ColorBackground(colorName));
    }

    public YAL fgColor(final String colorName)
    {
        return addStyle(new ColorForeground(colorName));
    }

    public YAL bgColor(final String colorName)
    {
        return addStyle(new ColorBackground(colorName));
    }

    public YAL fgGray()
    {
        return fgColor(E_ColorName.GRAY);
    }

    public YAL fgPink()
    {
        return fgColor(E_ColorName.PINK);
    }

    public YAL fgOrange()
    {
        return fgColor(E_ColorName.ORANGE);
    }

    public YAL fgBlue()
    {
        return fgColor(E_ColorName.BLUE);
    }

    public YAL fgRed()
    {
        return fgColor(E_ColorName.RED);
    }

    public YAL fgGreen()
    {
        return fgColor(E_ColorName.GREEN);
    }

    public YAL fgYellow()
    {
        return fgColor(E_ColorName.YELLOW);
    }

    public YAL fgCyan()
    {
        return fgColor(E_ColorName.CYAN);
    }

    public YAL fgMagenta()
    {
        return fgColor(E_ColorName.MAGENTA);
    }

    public YAL fgWhite()
    {
        return fgColor(E_ColorName.WHITE);
    }

    public YAL fgBlack()
    {
        return fgColor(E_ColorName.BLACK);
    }

    public YAL bgGray()
    {
        return bgColor(E_ColorName.GRAY);
    }

    public YAL bgPink()
    {
        return bgColor(E_ColorName.PINK);
    }

    public YAL bgOrange()
    {
        return bgColor(E_ColorName.ORANGE);
    }

    public YAL bgBlue()
    {
        return bgColor(E_ColorName.BLUE);
    }

    public YAL bgRed()
    {
        return bgColor(E_ColorName.RED);
    }

    public YAL bgGreen()
    {
        return bgColor(E_ColorName.GREEN);
    }

    public YAL bgYellow()
    {
        return bgColor(E_ColorName.YELLOW);
    }

    public YAL bgCyan()
    {
        return bgColor(E_ColorName.CYAN);
    }

    public YAL bgMagenta()
    {
        return bgColor(E_ColorName.MAGENTA);
    }

    public YAL bgWhite()
    {
        return bgColor(E_ColorName.WHITE);
    }

    public YAL bgBlack()
    {
        return bgColor(E_ColorName.BLACK);
    }
}
