package com.gitlab.debry.yal.input.array;

import com.gitlab.debry.yal.input.E_Input;
import com.gitlab.debry.yal.input.Input;
import java.util.Map;
import java.util.HashMap;

public abstract class Array extends Input
{
    @SuppressWarnings("serial")
    protected static Map<E_Value, String> defaultFormat = new HashMap<E_Value, String>()
    {
        {
            put(E_Value.INTEGER, "%%%dd");
            put(E_Value.DOUBLE, "%%%df");
        }
    };
    protected E_Array arrayType;

    protected E_Value valueType;

    protected Object[] value;

    public Array(final E_Array arrayType, final E_Value valueType, final Object[] value)
    {
        super(E_Input.ARRAY, null);
        this.arrayType = arrayType;
        this.valueType = valueType;
        this.value = value;
    }

    public String getFormat()
    {
        Object maxValue;
        Integer maxSign = 0;
        if (valueType == E_Value.INTEGER)
        {
            maxValue = Integer.valueOf(0);
            for (Object object : value)
                if (Math.abs((Integer) object) > ((Integer) maxValue))
                {
                    maxValue = Math.abs((Integer) object);
                    maxSign = ((Integer) object) < 0 ? 1 : 0;
                }
        }
        else
        {
            maxValue = Double.valueOf(0);
            for (Object object : value)
                if (Math.abs((Double) object) > ((Double) maxValue))
                {
                    maxValue = Math.abs((Double) object);
                    maxSign = ((Double) object) < 0 ? 1 : 0;
                }
        }

        final int maxDigit = String.valueOf(maxValue).length() + maxSign;
        return " " + getFormat(String.format(defaultFormat.get(valueType), maxDigit)) + " ";
    }
}
