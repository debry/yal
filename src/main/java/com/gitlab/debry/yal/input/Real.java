package com.gitlab.debry.yal.input;

import java.util.Locale;

public class Real extends Input
{
    static private String defaultFormat = "%f";

    public Real(final double value)
    {
        super(E_Input.REAL, Double.valueOf(value));
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        Real.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(Locale.US, getFormat(), value);
    }
}
