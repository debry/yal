package com.gitlab.debry.yal.input.stack;

public class StackAll extends Stack
{
    private static String defaultFormat = "%s";

    public StackAll()
    {
        super(E_Stack.ALL);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        StackAll.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), ((StackTraceElement) value).toString());
    }
}
