package com.gitlab.debry.yal.input.stack;

import com.gitlab.debry.yal.input.Input;
import com.gitlab.debry.yal.input.E_Input;

public abstract class Stack extends Input
{
    private static final int STACK_TRACE_LEVELS_UP = 4;

    private E_Stack type;

    public Stack(final E_Stack type)
    {
        super(E_Input.STACK, Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP]);
        this.type = type;

    }

    public E_Stack getStackType()
    {
        return type;
    }
}
