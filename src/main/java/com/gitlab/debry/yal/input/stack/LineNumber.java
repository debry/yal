package com.gitlab.debry.yal.input.stack;

public class LineNumber extends Stack
{
    private static String defaultFormat = "%d";

    public LineNumber()
    {
        super(E_Stack.LINE);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        LineNumber.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), ((StackTraceElement) value).getLineNumber());
    }
}
