package com.gitlab.debry.yal.input;

public class Separator extends Input
{
    static private String defaultSeparator = ":";

    public Separator()
    {
        super(E_Input.SEP, defaultSeparator);
    }

    public Separator(final String separator)
    {
        super(E_Input.SEP, separator);
    }

    public static String getSeparator()
    {
        return defaultSeparator;
    }

    public static void setSeparator(final String defaultSeparator)
    {
        Separator.defaultSeparator = defaultSeparator;
    }

    @Override
    public String toString()
    {
        return (String) value;
    }

}
