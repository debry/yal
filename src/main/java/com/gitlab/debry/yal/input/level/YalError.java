package com.gitlab.debry.yal.input.level;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.style.Style;
import java.util.List;

public class YalError extends Level
{
    static private final int level = 4096;

    static private final String defaultFormat = "%s";

    static final private String resourceFile = "/config/input/level/error";

    static private List<Style> defaultStyle = initStyle(YalError.class.getResourceAsStream(resourceFile));

    public static void configFromFile(final String configFile)
    {
        defaultStyle.clear();
        styleFromMap(Config.fromFile(configFile), defaultStyle);
    }

    public static void configFromString(final String configString)
    {
        defaultStyle.clear();
        styleFromMap(Config.fromString(configString), defaultStyle);
    }

    public YalError()
    {
        super("ERROR", E_Level.ERROR, level);
        setStyle(defaultStyle);
    }

    @Override
    public String toString()
    {
        return String.format(defaultFormat, value);
    }

    static public int getStaticLevel()
    {
        return level;
    }
}
