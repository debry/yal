package com.gitlab.debry.yal.input;

public class Str extends Input
{
    private static String defaultFormat = "%s";

    public Str(final String value)
    {
        super(E_Input.STR, value);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        Str.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), value);
    }
}
