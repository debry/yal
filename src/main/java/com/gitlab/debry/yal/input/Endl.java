package com.gitlab.debry.yal.input;

public class Endl extends Input
{
    public Endl()
    {
        super(E_Input.ENDL, "\n");
    }

    @Override
    public String toString()
    {
        return (String) value;
    }
}
