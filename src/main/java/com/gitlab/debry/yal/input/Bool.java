package com.gitlab.debry.yal.input;

public class Bool extends Input
{
    static private String defaultFormat = "true|false";

    public Bool(final boolean value)
    {
        super(E_Input.BOOL, Boolean.valueOf(value));
    }

    @Override
    public String toString()
    {
        final String[] fmt = getFormat().split("\\|", 2);
        return ((Boolean) value) ? fmt[0] : fmt[1];
    }

    public String getFormat()
    {
        final String format = getFormat(defaultFormat);
        return format.contains("|") ? format : defaultFormat;

    }

    public static void setFormat(final String defaultFormat)
    {
        if (defaultFormat.contains("|"))
            Bool.defaultFormat = defaultFormat;
    }
}
