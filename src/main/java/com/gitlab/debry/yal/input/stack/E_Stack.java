package com.gitlab.debry.yal.input.stack;

public enum E_Stack
{
    LINE,
    FILE,
    METHOD,
    CLASS,
    ALL
}
