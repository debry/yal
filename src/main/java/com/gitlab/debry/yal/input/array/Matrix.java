package com.gitlab.debry.yal.input.array;

import java.util.Arrays;
import java.util.Locale;

public class Matrix extends Array
{
    private int rows;

    private int cols;

    private E_MatrixOrder matrixOrder;

    private void setDimension(final int rows, final int cols, final E_MatrixOrder matrixOrder)
            throws IllegalArgumentException
    {
        if (rows * cols > value.length)
            throw new IllegalArgumentException(
                    String.format("rows (%d) x cols (%d) exceeds the array length (%d)",
                            rows, cols, value.length));

        this.rows = rows;
        this.cols = cols;
        this.matrixOrder = matrixOrder;
    }

    public Matrix(final int[] value, final int rows, final int cols)
    {
        super(E_Array.MATRIX, E_Value.INTEGER, Arrays.stream(value).boxed().toArray(Integer[]::new));
        setDimension(rows, cols, E_MatrixOrder.ROW_MAJOR);
    }

    public Matrix(final double[] value, final int rows, final int cols)
    {
        super(E_Array.MATRIX, E_Value.DOUBLE, Arrays.stream(value).boxed().toArray(Double[]::new));
        setDimension(rows, cols, E_MatrixOrder.ROW_MAJOR);
    }

    public Matrix(final int[] value, final int rows, final int cols,
            final E_MatrixOrder matrixOrder)
    {
        super(E_Array.MATRIX, E_Value.INTEGER, Arrays.stream(value).boxed().toArray(Integer[]::new));
        setDimension(rows, cols, matrixOrder);
    }

    public Matrix(final double[] value, final int rows, final int cols,
            final E_MatrixOrder matrixOrder)
    {
        super(E_Array.MATRIX, E_Value.DOUBLE, Arrays.stream(value).boxed().toArray(Double[]::new));
        setDimension(rows, cols, matrixOrder);
    }

    private int getp(final int row, final int col)
    {
        return matrixOrder == E_MatrixOrder.ROW_MAJOR ? (row * cols + col) : (col * rows + row);
    }

    @Override
    public String toString()
    {
        final StringBuilder matStr = new StringBuilder();

        matStr.append("\n[ ");
        for (int i = 0; i < rows; ++i)
        {
            matStr.append(i == 0 ? "[" : "  [");
            for (int j = 0; j < cols; ++j)
                matStr.append(String.format(Locale.US, getFormat(), value[getp(i, j)]));
            matStr.append(i < rows - 1 ? "]\n" : "]");
        }
        matStr.append(String.format(" ] (%d, %d)", rows, cols));

        return matStr.toString();
    }
}
