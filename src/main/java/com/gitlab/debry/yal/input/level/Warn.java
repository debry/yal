package com.gitlab.debry.yal.input.level;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.style.Style;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Warn extends Level
{
    static private final int[] level = { 256, 512, 1024, 2048 };

    static private final String defaultFormat = "WARN%d";

    static final private String[] resourceFile = {
            "/config/input/level/warn/0",
            "/config/input/level/warn/1",
            "/config/input/level/warn/2",
            "/config/input/level/warn/3"
    };

    static private ArrayList<List<Style>> defaultStyle = new ArrayList<List<Style>>(
            Arrays.asList(
                    initStyle(Warn.class.getResourceAsStream(resourceFile[0])),
                    initStyle(Warn.class.getResourceAsStream(resourceFile[1])),
                    initStyle(Warn.class.getResourceAsStream(resourceFile[2])),
                    initStyle(Warn.class.getResourceAsStream(resourceFile[3]))));

    public static void configFromFile(final int subLevel, final String configFile)
    {
        defaultStyle.get(subLevel).clear();
        styleFromMap(Config.fromFile(configFile), defaultStyle.get(subLevel));
    }

    public static void configFromString(final int subLevel, final String configString)
    {
        defaultStyle.get(subLevel).clear();
        styleFromMap(Config.fromString(configString), defaultStyle.get(subLevel));
    }

    public Warn(final int subLevel)
    {
        super(Integer.valueOf(subLevel), E_Level.WARN, level[subLevel]);
        setStyle(defaultStyle.get(subLevel));
    }

    @Override
    public String toString()
    {
        return String.format(defaultFormat, value);
    }

    static public int getStaticLevel()
    {
        return level[0] | level[1] | level[2] | level[3];
    }

    static public int getStaticLevel(final int subLevel)
    {
        return level[subLevel];
    }
}
