package com.gitlab.debry.yal.input.stack;

public class FileName extends Stack
{
    private static String defaultFormat = "%s";

    public FileName()
    {
        super(E_Stack.FILE);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        FileName.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), ((StackTraceElement) value).getFileName());
    }
}
