package com.gitlab.debry.yal.input.level;

import java.util.List;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.style.Style;

public class Fatal extends Level
{
    static private final int level = 8192;

    static private final String defaultFormat = "%s";

    static final private String resourceFile = "/config/input/level/fatal";

    static private List<Style> defaultStyle = initStyle(Fatal.class.getResourceAsStream(resourceFile));

    public static void configFromFile(final String configFile)
    {
        defaultStyle.clear();
        styleFromMap(Config.fromFile(configFile), defaultStyle);
    }

    public static void configFromString(final String configString)
    {
        defaultStyle.clear();
        styleFromMap(Config.fromString(configString), defaultStyle);
    }

    public Fatal()
    {
        super("FATAL", E_Level.FATAL, level);
        setStyle(defaultStyle);
    }

    @Override
    public String toString()
    {
        return String.format(defaultFormat, value);
    }

    static public int getStaticLevel()
    {
        return level;
    }
}
