package com.gitlab.debry.yal.input;

import java.util.Date;
import java.text.SimpleDateFormat;

public class Time extends Input
{
    static private String defaultFormat = "HH'h'mm ss.SSS";

    public Time()
    {
        super(E_Input.TIME, new Date());
    }

    @Override
    public String toString()
    {
        return (new SimpleDateFormat(getFormat())).format(value);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        Time.defaultFormat = defaultFormat;
    }
}
