package com.gitlab.debry.yal.input;

public class Space extends Input
{
    private static char spaceCharacter = ' ';

    public static char getCharacter()
    {
        return spaceCharacter;
    }

    public static void setCharacter(final char spaceCharacter)
    {
        Space.spaceCharacter = spaceCharacter;
    }

    public Space(final int value)
    {
        super(E_Input.SPACE, Integer.valueOf(value));
    }

    public Space()
    {
        this(1);
    }

    @Override
    public String toString()
    {
        final int n = ((Integer) value).intValue();
        return new String(new char[n]).replace('\0', spaceCharacter);
    }
}
