package com.gitlab.debry.yal.input.level;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.style.Style;

public class Info extends Level
{
    static private final int[] level = { 1, 2, 4, 8 };

    static private final String defaultFormat = "INFO%d";

    static final private String[] resourceFile = {
            "/config/input/level/info/0",
            "/config/input/level/info/1",
            "/config/input/level/info/2",
            "/config/input/level/info/3"
    };

    static private ArrayList<List<Style>> defaultStyle = new ArrayList<List<Style>>(
            Arrays.asList(
                    initStyle(Info.class.getResourceAsStream(resourceFile[0])),
                    initStyle(Info.class.getResourceAsStream(resourceFile[1])),
                    initStyle(Info.class.getResourceAsStream(resourceFile[2])),
                    initStyle(Info.class.getResourceAsStream(resourceFile[3]))));

    public static void configFromFile(final int subLevel, final String configFile)
    {
        defaultStyle.get(subLevel).clear();
        styleFromMap(Config.fromFile(configFile), defaultStyle.get(subLevel));
    }

    public static void configFromString(final int subLevel, final String configString)
    {
        defaultStyle.get(subLevel).clear();
        styleFromMap(Config.fromString(configString), defaultStyle.get(subLevel));
    }

    public Info(final int subLevel)
    {
        super(Integer.valueOf(subLevel), E_Level.INFO, level[subLevel]);
        setStyle(defaultStyle.get(subLevel));
    }

    @Override
    public String toString()
    {
        return String.format(defaultFormat, value);
    }

    static public int getStaticLevel()
    {
        return level[0] | level[1] | level[2] | level[3];
    }

    static public int getStaticLevel(final int subLevel)
    {
        return level[subLevel];
    }
}
