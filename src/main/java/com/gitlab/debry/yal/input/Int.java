package com.gitlab.debry.yal.input;

public class Int extends Input
{
    private static String defaultFormat = "%d";

    public Int(final int value)
    {
        super(E_Input.INT, Integer.valueOf(value));
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        Int.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), value);
    }
}
