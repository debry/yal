package com.gitlab.debry.yal.input;

public class Obj extends Input
{
    private static String defaultFormat = "%s";

    public Obj(final Object value)
    {
        super(E_Input.OBJ, value);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        Obj.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), value.toString());
    }

}
