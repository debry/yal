package com.gitlab.debry.yal.input.level;

public class GetLevel
{
    static public int withType(final E_Level levelType)
    {
        switch (levelType)
        {
            case DEBUG:
                return Debug.getStaticLevel();
            case INFO:
                return Info.getStaticLevel();
            case WARN:
                return Warn.getStaticLevel();
            case ERROR:
                return YalError.getStaticLevel();
            case FATAL:
                return Fatal.getStaticLevel();
            case USER:
                return User.getStaticLevel();
            case ALL:
                return Debug.getStaticLevel() |
                        Info.getStaticLevel() |
                        Warn.getStaticLevel() |
                        YalError.getStaticLevel() |
                        Fatal.getStaticLevel() |
                        User.getStaticLevel();
            case ALL_BUT_DEBUG:
                return Info.getStaticLevel() |
                        Warn.getStaticLevel() |
                        YalError.getStaticLevel() |
                        Fatal.getStaticLevel() |
                        User.getStaticLevel();
            default:
                return 0;
        }
    }

    static public int withTypeAndSubLevel(final E_Level levelType, final int subLevel)
    {
        switch (levelType)
        {
            case DEBUG:
                return Debug.getStaticLevel(subLevel);
            case INFO:
                return Info.getStaticLevel(subLevel);
            case WARN:
                return Warn.getStaticLevel(subLevel);
            case USER:
                return User.getStaticLevel(subLevel);
            case ALL:
                return Debug.getStaticLevel(subLevel) |
                        Info.getStaticLevel(subLevel) |
                        Warn.getStaticLevel(subLevel) |
                        User.getStaticLevel(subLevel);
            case ALL_BUT_DEBUG:
                return Info.getStaticLevel(subLevel) |
                        Warn.getStaticLevel(subLevel) |
                        User.getStaticLevel(subLevel);
            default:
                return 0;
        }
    }
}
