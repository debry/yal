package com.gitlab.debry.yal.input.stack;

public class MethodName extends Stack
{
    private static String defaultFormat = "%s";

    public MethodName()
    {
        super(E_Stack.METHOD);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        MethodName.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), ((StackTraceElement) value).getMethodName());
    }
}
