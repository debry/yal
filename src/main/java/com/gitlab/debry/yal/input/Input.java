package com.gitlab.debry.yal.input;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.style.E_Style;
import com.gitlab.debry.yal.style.Format;
import com.gitlab.debry.yal.style.Style;
import com.gitlab.debry.yal.style.color.ColorBackground;
import com.gitlab.debry.yal.style.color.ColorForeground;
import com.gitlab.debry.yal.style.color.E_ColorName;
import com.gitlab.debry.yal.style.decoration.DecorationBlink;
import com.gitlab.debry.yal.style.decoration.DecorationStrikeThrough;
import com.gitlab.debry.yal.style.decoration.DecorationUnderline;
import com.gitlab.debry.yal.style.font.E_FontWeight;
import com.gitlab.debry.yal.style.font.FontFamily;
import com.gitlab.debry.yal.style.font.FontSize;
import com.gitlab.debry.yal.style.font.FontWeight;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class Input
{
    private final E_Input type;

    protected Object value;

    protected List<Style> style;

    private Input next;

    protected static void styleFromMap(final Map<String, Object> configMap, final List<Style> style)
    {
        Style.fromMap(Config.filter(configMap, "style"), style);
    }

    protected static void styleFromFile(final String configFile, final List<Style> style)
    {
        styleFromMap(Config.fromFile(configFile), style);
    }

    protected static void styleFromString(final String configString, final List<Style> style)
    {
        styleFromMap(Config.fromString(configString), style);
    }

    protected static List<Style> initStyle(final InputStream resourceStream)
    {
        final List<Style> style = new LinkedList<Style>();

        if (resourceStream != null)
            styleFromString(new BufferedReader(
                    new InputStreamReader(resourceStream, StandardCharsets.UTF_8))
                            .lines()
                            .collect(Collectors.joining("\n")),
                    style);

        return style;
    }

    protected void setStyle(final List<Style> defaultStyle)
    {
        if (!defaultStyle.isEmpty())
            Style.deepCopy(defaultStyle, style);
    }

    public Input(final E_Input type, final Object value)
    {
        this.type = type;
        this.value = value;
        next = null;
        style = new LinkedList<Style>();
    }

    public Object getValue()
    {
        return value;
    }

    public E_Input getType()
    {
        return type;
    }

    public Input setNext(final Input next)
    {
        this.next = next;
        return this.next;
    }

    public Input getNext()
    {
        return next;
    }

    public List<Style> getStyle()
    {
        return style;
    }

    public void addStyle(final Style style)
    {
        Style.addReplace(style, this.style);
    }

    public Input format(final String format)
    {
        Style.addReplace(new Format(format), style);
        return this;
    }

    public Input fontFamily(final String fontName)
    {
        Style.addReplace(new FontFamily(fontName), style);
        return this;
    }

    public Input fontWeight(final E_FontWeight fontWeight)
    {
        Style.addReplace(new FontWeight(fontWeight), style);
        return this;
    }

    public Input fontSize(final int fontSize)
    {
        Style.addReplace(new FontSize(fontSize), style);
        return this;
    }

    public Input underLine()
    {
        Style.addReplace(new DecorationUnderline(), style);
        return this;
    }

    public Input blink()
    {
        Style.addReplace(new DecorationBlink(), style);
        return this;
    }

    public Input strikeThrough()
    {
        Style.addReplace(new DecorationStrikeThrough(), style);
        return this;
    }

    public Input fgColor(final E_ColorName colorName)
    {
        Style.addReplace(new ColorForeground(colorName), style);
        return this;
    }

    public Input bgColor(final E_ColorName colorName)
    {
        Style.addReplace(new ColorBackground(colorName), style);
        return this;
    }

    public Input fgColor(final String colorName)
    {
        Style.addReplace(new ColorForeground(colorName), style);
        return this;
    }

    public Input bgColor(final String colorName)
    {
        Style.addReplace(new ColorBackground(colorName), style);
        return this;
    }

    public Input fgGray()
    {
        return fgColor(E_ColorName.GRAY);
    }

    public Input fgPink()
    {
        return fgColor(E_ColorName.PINK);
    }

    public Input fgOrange()
    {
        return fgColor(E_ColorName.ORANGE);
    }

    public Input fgBlue()
    {
        return fgColor(E_ColorName.BLUE);
    }

    public Input fgRed()
    {
        return fgColor(E_ColorName.RED);
    }

    public Input fgGreen()
    {
        return fgColor(E_ColorName.GREEN);
    }

    public Input fgYellow()
    {
        return fgColor(E_ColorName.YELLOW);
    }

    public Input fgCyan()
    {
        return fgColor(E_ColorName.CYAN);
    }

    public Input fgMagenta()
    {
        return fgColor(E_ColorName.MAGENTA);
    }

    public Input fgWhite()
    {
        return fgColor(E_ColorName.WHITE);
    }

    public Input fgBlack()
    {
        return fgColor(E_ColorName.BLACK);
    }

    public Input bgGray()
    {
        return bgColor(E_ColorName.GRAY);
    }

    public Input bgPink()
    {
        return bgColor(E_ColorName.PINK);
    }

    public Input bgOrange()
    {
        return bgColor(E_ColorName.ORANGE);
    }

    public Input bgBlue()
    {
        return bgColor(E_ColorName.BLUE);
    }

    public Input bgRed()
    {
        return bgColor(E_ColorName.RED);
    }

    public Input bgGreen()
    {
        return bgColor(E_ColorName.GREEN);
    }

    public Input bgYellow()
    {
        return bgColor(E_ColorName.YELLOW);
    }

    public Input bgCyan()
    {
        return bgColor(E_ColorName.CYAN);
    }

    public Input bgMagenta()
    {
        return bgColor(E_ColorName.MAGENTA);
    }

    public Input bgWhite()
    {
        return bgColor(E_ColorName.WHITE);
    }

    public Input bgBlack()
    {
        return bgColor(E_ColorName.BLACK);
    }

    protected String getFormat(final String defaultFormat)
    {
        for (final Style st : style)
            if (st.getType() == E_Style.FORMAT)
                return ((Format) st).getFormat();
        return defaultFormat;
    }
}
