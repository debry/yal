package com.gitlab.debry.yal.input;

public enum E_Input
{
    STACK,
    SEP,
    ENDL,
    SPACE,
    LEVEL,
    STR,
    INT,
    REAL,
    BOOL,
    TIME,
    ARRAY,
    OBJ
}
