package com.gitlab.debry.yal.input.level;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.style.Style;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class User extends Level
{
    static private final int[] level = { 16384, 32768, 65536, 131072, 262144 };

    static private String[] defaultFormat = { "USER0", "USER1", "USER2", "USER3", "USER4" };

    static final private String[] resourceFile = {
            "/config/input/level/user/0",
            "/config/input/level/user/1",
            "/config/input/level/user/2",
            "/config/input/level/user/3",
            "/config/input/level/user/4"
    };

    static private ArrayList<List<Style>> defaultStyle = new ArrayList<List<Style>>(
            Arrays.asList(
                    initStyle(User.class.getResourceAsStream(resourceFile[0])),
                    initStyle(User.class.getResourceAsStream(resourceFile[1])),
                    initStyle(User.class.getResourceAsStream(resourceFile[2])),
                    initStyle(User.class.getResourceAsStream(resourceFile[3])),
                    initStyle(User.class.getResourceAsStream(resourceFile[4]))));

    public static void configFromFile(final int subLevel, final String configFile)
    {
        defaultStyle.get(subLevel).clear();
        styleFromMap(Config.fromFile(configFile), defaultStyle.get(subLevel));
    }

    public static void configFromString(final int subLevel, final String configString)
    {
        defaultStyle.get(subLevel).clear();
        styleFromMap(Config.fromString(configString), defaultStyle.get(subLevel));
    }

    public User(final int subLevel)
    {
        super(Integer.valueOf(subLevel), E_Level.USER, level[subLevel]);
        setStyle(defaultStyle.get(subLevel));
    }

    @Override
    public String toString()
    {
        return defaultFormat[(int) value];
    }

    public static String getFormat(final int subLevel)
    {
        return defaultFormat[subLevel];
    }

    public static void setFormat(final int subLevel, final String defaultFormatSubLevel)
    {
        User.defaultFormat[subLevel] = defaultFormatSubLevel;
    }

    static public int getStaticLevel()
    {
        return level[0] | level[1] | level[2] | level[3] | level[4];
    }

    static public int getStaticLevel(final int subLevel)
    {
        return level[subLevel];
    }
}
