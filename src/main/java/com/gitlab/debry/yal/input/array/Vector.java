package com.gitlab.debry.yal.input.array;

import java.util.Arrays;
import java.util.Locale;

public class Vector extends Array
{
    private final int begin;
    private final int end;

    public Vector(final int[] value)
    {
        super(E_Array.VECTOR, E_Value.INTEGER, Arrays.stream(value).boxed().toArray(Integer[]::new));
        begin = 0;
        end = value.length;
    }

    public Vector(final int[] value, final int begin, final int end)
    {
        super(E_Array.VECTOR, E_Value.INTEGER, Arrays.stream(value).boxed().toArray(Integer[]::new));
        this.begin = begin;
        this.end = end;
    }

    public Vector(final double[] value)
    {
        super(E_Array.VECTOR, E_Value.DOUBLE, Arrays.stream(value).boxed().toArray(Double[]::new));
        begin = 0;
        end = value.length;
    }

    public Vector(final double[] value, final int begin, final int end)
    {
        super(E_Array.VECTOR, E_Value.DOUBLE, Arrays.stream(value).boxed().toArray(Double[]::new));
        this.begin = begin > 0 ? begin : 0;
        this.end = end < value.length ? end : value.length;
    }

    @Override
    public String toString()
    {
        final StringBuilder vecStr = new StringBuilder();

        vecStr.append(begin > 0 ? "[ …" : "[");
        for (int i = begin; i < end; ++i)
            vecStr.append(String.format(Locale.US, getFormat(), value[i]));
        vecStr.append(String.format("%s (%d, %d)", end < value.length ? "… ]" : "]", begin, end - 1));

        return vecStr.toString();
    }
}
