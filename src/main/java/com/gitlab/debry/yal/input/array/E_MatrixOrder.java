package com.gitlab.debry.yal.input.array;

public enum E_MatrixOrder
{
    ROW_MAJOR,
    COLUMN_MAJOR;
}
