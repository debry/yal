package com.gitlab.debry.yal.input.level;

public enum E_Level
{
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL,
    USER,
    ALL,
    ALL_BUT_DEBUG
}
