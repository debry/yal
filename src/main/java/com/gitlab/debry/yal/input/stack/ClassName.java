package com.gitlab.debry.yal.input.stack;

public class ClassName extends Stack
{
    private static String defaultFormat = "%s";

    public ClassName()
    {
        super(E_Stack.CLASS);
    }

    public String getFormat()
    {
        return getFormat(defaultFormat);
    }

    public static void setFormat(final String defaultFormat)
    {
        ClassName.defaultFormat = defaultFormat;
    }

    @Override
    public String toString()
    {
        return String.format(getFormat(), ((StackTraceElement) value).getClassName());
    }
}
