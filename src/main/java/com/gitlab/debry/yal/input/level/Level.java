package com.gitlab.debry.yal.input.level;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gitlab.debry.yal.input.E_Input;
import com.gitlab.debry.yal.input.Input;

public abstract class Level extends Input
{
    private final int level;

    private final E_Level levelType;

    public Level(final Object value, final E_Level levelType, final int level)
    {
        super(E_Input.LEVEL, value);
        this.levelType = levelType;
        this.level = level;
    }

    public int getLevel()
    {
        return level;
    }

    public E_Level getLevelType()
    {
        return levelType;
    }

    public static int fromMap(final Map<String, Object> levelMap)
    {
        int level = 0;

        if (!levelMap.isEmpty())
        {
            for (final Map.Entry<String, Object> entry : levelMap.entrySet())
            {
                final E_Level levelType = E_Level.valueOf(entry.getKey().toUpperCase());

                if (entry.getValue() instanceof Boolean)
                {
                    if ((Boolean) entry.getValue())
                        level = level | GetLevel.withType(levelType);
                }
                else if (entry.getValue() instanceof String)
                {
                    final Pattern patternSubLevel = Pattern.compile("[0-9]+");
                    final Matcher matcherSubLevel = patternSubLevel.matcher(((String) entry.getValue()));

                    while (matcherSubLevel.find())
                        level = level | GetLevel.withTypeAndSubLevel(levelType,
                                Integer.parseInt(matcherSubLevel.group()));
                }
            }
        }

        return level;
    }
}
