package com.gitlab.debry.yal.output;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.gitlab.debry.yal.config.Config;
import com.gitlab.debry.yal.input.Endl;
import com.gitlab.debry.yal.input.Input;
import com.gitlab.debry.yal.input.Str;
import com.gitlab.debry.yal.input.array.Vector;
import com.gitlab.debry.yal.input.level.E_Level;
import com.gitlab.debry.yal.input.level.GetLevel;
import com.gitlab.debry.yal.input.level.Level;
import com.gitlab.debry.yal.style.Style;
import com.gitlab.debry.yal.style.color.ColorBackground;
import com.gitlab.debry.yal.style.color.ColorForeground;
import com.gitlab.debry.yal.style.color.E_ColorName;
import com.gitlab.debry.yal.style.decoration.Decoration;
import com.gitlab.debry.yal.style.font.E_FontWeight;
import com.gitlab.debry.yal.style.font.FontFamily;
import com.gitlab.debry.yal.style.font.FontSize;
import com.gitlab.debry.yal.style.font.FontWeight;

public class JConsole
{
    static final private String resourceFile = "/config/output/jconsole";

    static final int defaultJConsoleFrameWidth = 800;
    static final int defaultJConsoleFrameHeight = 600;

    private final String name;

    private int level;

    private final List<com.gitlab.debry.yal.style.Style> style;

    private final JTextPane textPane;

    private final StyledDocument doc;

    private final JFrame frame;

    public JConsole(final String name)
    {
        this.name = name;
        level = 0;
        style = new LinkedList<com.gitlab.debry.yal.style.Style>();

        textPane = new JTextPane();
        textPane.setEditable(false);
        doc = textPane.getStyledDocument();

        final JScrollPane scrollPane = new JScrollPane(textPane);

        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        frame = new JFrame(name);
        frame.getContentPane().add(scrollPane);
        frame.pack();

        final InputStream inputStream = getClass().getResourceAsStream(resourceFile);
        if (inputStream != null)
            configFromString(new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n")));
    }

    public JConsole configFromFile(final String configFile)
    {
        configFromMap(Config.fromFile(configFile));
        return this;
    }

    public JConsole configFromString(final String configString)
    {
        configFromMap(Config.fromString(configString));
        return this;
    }

    public String getName()
    {
        return name;
    }

    public JConsole addLevel(final E_Level levelType, final int... subLevels)
    {
        if (subLevels.length > 0)
        {
            for (final int subLevel : subLevels)
                level = level | GetLevel.withTypeAndSubLevel(levelType, subLevel);
        }
        else
            level = level | GetLevel.withType(levelType);
        return this;
    }

    public int getLevel()
    {
        return level;
    }

    public JConsole fgColor(final E_ColorName colorName)
    {
        Style.addReplace(new ColorForeground(colorName), style);
        return this;
    }

    public JConsole bgColor(final E_ColorName colorName)
    {
        Style.addReplace(new ColorBackground(colorName), style);
        return this;
    }

    private void configParse(final String key, final Object object)
    {
        final String[] keyArray = key.split("\\.");

        if (keyArray[0].equals("color"))
        {
            final int[] rgb = com.gitlab.debry.yal.style.color.Color.nameToRGB.get(E_ColorName.valueOf(((String) object).toUpperCase()));

            if (keyArray[1].equals("background"))
                textPane.setBackground(new Color(rgb[0], rgb[1], rgb[2]));
            else if (keyArray[1].equals("foreground"))
                textPane.setForeground(new Color(rgb[0], rgb[1], rgb[2]));
        }
        else if (keyArray[0].equals("font"))
        {
            final Font font = textPane.getFont();

            if (keyArray[1].equals("name"))
                textPane.setFont(new Font(((String) object), font.getStyle(), font.getSize()));
            else if (keyArray[1].equals("size"))
                textPane.setFont(new Font(font.getName(), font.getStyle(), ((Integer) object).intValue()));
        }
        else if (keyArray[0].equals("size"))
        {
            final Pattern patternInt = Pattern.compile("[0-9]+");
            final Matcher matcherInt = patternInt.matcher(((String) object));

            final int width = matcherInt.find() ? Integer.parseInt(matcherInt.group()) : defaultJConsoleFrameWidth;
            final int height = matcherInt.find() ? Integer.parseInt(matcherInt.group()) : defaultJConsoleFrameHeight;

            frame.setSize(new Dimension(width, height));
        }
        else if (keyArray[0].equals("visible"))
        {
            frame.setVisible(((Boolean) object).booleanValue());
        }
        else if (keyArray[0].equals("defaultcloseoperation"))
        {
            if (((String) object).equals("exit_on_close"))
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }

    protected void configFromMap(final Map<String, Object> configMap)
    {
        Style.fromMap(Config.filter(configMap, "style"), style);
        level = Level.fromMap(Config.filter(configMap, "level"));

        for (final Map.Entry<String, Object> entry : Config.filter(configMap, "jconsole").entrySet())
            configParse(entry.getKey(), entry.getValue());
    }

    public JConsole setBackground(final String hexColor)
    {
        textPane.setBackground(Color.decode(hexColor));
        return this;
    }

    public JConsole setForeground(final String hexColor)
    {
        textPane.setForeground(Color.decode(hexColor));
        return this;
    }

    public JConsole setFontSize(final int fontSize)
    {
        final Font font = textPane.getFont();
        textPane.setFont(new Font(font.getName(), font.getStyle(), fontSize));
        return this;
    }

    public JConsole setFontName(final String fontName)
    {
        final Font font = textPane.getFont();
        textPane.setFont(new Font(fontName, font.getStyle(), font.getSize()));
        return this;
    }

    public JConsole setSize(final int width, final int height)
    {
        frame.setSize(new Dimension(width, height));
        return this;
    }

    public JConsole toggleVisible()
    {
        frame.setVisible(frame.isVisible() ? false : true);
        return this;
    }

    public void listColor()
    {
        insert((new Str("========== Available colors =========="))
                .fontSize(textPane.getFont().getSize() + 10)
                .fontWeight(E_FontWeight.BOLD));
        insert(new Endl());

        for (final Map.Entry<E_ColorName, int[]> entry : com.gitlab.debry.yal.style.color.Color.nameToRGB.entrySet())
        {
            final int[] rgb = entry.getValue();
            final float y = (rgb[0] / 255f) * 0.2126f + (rgb[1] / 255f) * 0.7152f
                    + (rgb[2] / 255f) * 0.0722f, Y = 0.25f;
            insert((new Str(entry.getKey().name()))
                    .fgColor(y > Y ? "black" : "white")
                    .bgColor(entry.getKey())
                    .format("%30s : "));
            insert((new Vector(entry.getValue()))
                    .fgColor(y > Y ? "black" : "white")
                    .bgColor(entry.getKey())
                    .format("%03d"));
            insert(new Endl());
        }
    }

    public void listAvailableFontFamilyName()
    {
        final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

        final String[] nameFontFamily = ge.getAvailableFontFamilyNames();

        insert((new Str("======== Available font family names ========"))
                .fontSize(textPane.getFont().getSize() + 10)
                .fontWeight(E_FontWeight.BOLD));
        insert(new Endl());
        for (final String name : nameFontFamily)
        {
            insert((new Str(name)).format("%30s : "));
            insert((new Str("Hello world")).format("\"%s\"").fontFamily(name));
            insert(new Endl());
        }
    }

    private void setFontStyle(final com.gitlab.debry.yal.style.font.Font st, final javax.swing.text.Style style)
    {
        switch (st.getFontStyle())
        {
            case FAMILY:
                StyleConstants.setFontFamily(style, ((FontFamily) st).getFontName());
                break;
            case WEIGHT:
                switch (((FontWeight) st).getFontWeight())
                {
                    case BOLD:
                        StyleConstants.setBold(style, true);
                        break;
                    case ITALIC:
                        StyleConstants.setItalic(style, true);
                        break;
                    case NORMAL:
                        break;
                    default:
                        break;
                }
                break;
            case SIZE:
                StyleConstants.setFontSize(style, ((FontSize) st).getFontSize());
        }
    }

    private void setColorStyle(final com.gitlab.debry.yal.style.color.Color st, final javax.swing.text.Style style)
    {
        try
        {
            final int[] rgb = com.gitlab.debry.yal.style.color.Color.nameToRGB.get(st.getColorName());

            switch (st.getColorStyle())
            {
                case FOREGROUND:
                    StyleConstants.setForeground(style, new Color(rgb[0], rgb[1], rgb[2]));
                    break;
                case BACKGROUND:
                    StyleConstants.setBackground(style, new Color(rgb[0], rgb[1], rgb[2]));
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setDecorationStyle(final Decoration st, final javax.swing.text.Style style)
    {
        switch (st.getDecorationStyle())
        {
            case UNDERLINE:
                StyleConstants.setUnderline(style, true);
                break;
            case STRIKETHROUGH:
                StyleConstants.setStrikeThrough(style, true);
            case BLINK:
                break;
            default:
                break;
        }
    }

    private javax.swing.text.Style getStyle(final Input input)
    {
        final javax.swing.text.Style style = textPane.addStyle("", null);

        for (final com.gitlab.debry.yal.style.Style st : com.gitlab.debry.yal.style.Style.merge(this.style, input.getStyle()))
        {
            switch (st.getType())
            {
                case FONT:
                    setFontStyle(((com.gitlab.debry.yal.style.font.Font) st), style);
                    break;
                case COLOR:
                    setColorStyle(((com.gitlab.debry.yal.style.color.Color) st), style);
                    break;
                case DECORATION:
                    setDecorationStyle(((com.gitlab.debry.yal.style.decoration.Decoration) st), style);
                case FORMAT:
                    break;
                default:
                    break;
            }
        }

        return style;
    }

    protected void insert(final Input input)
    {
        try
        {
            doc.insertString(doc.getLength(), input.toString(), getStyle(input));
        }
        catch (final BadLocationException e)
        {
        }
    }

    public void receive(final Input input)
    {
        Input inputPointer = input;

        while (inputPointer != null)
        {
            this.insert(inputPointer);
            inputPointer = inputPointer.getNext();
        }
    }
}
