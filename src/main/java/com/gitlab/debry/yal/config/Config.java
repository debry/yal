package com.gitlab.debry.yal.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Config
{
    public static void scanString(final String configString, final Map<String, Object> map)
    {
        final Scanner configScanner = new Scanner(configString);

        final Pattern patternComment = Pattern.compile("^\\s*(.*)\\s*#\\s.*$");
        final Pattern patternKeyValue = Pattern.compile("^\\s*(\\S+)\\s*[:=]\\s*(\\S.*\\S)\\s*$");

        while (configScanner.hasNextLine())
        {
            final String line = configScanner.nextLine();

            final Matcher matcherComment = patternComment.matcher(line);

            final Matcher matcherKeyValue = patternKeyValue.matcher(matcherComment.find() ? matcherComment.group() : line);

            if (matcherKeyValue.find() && matcherKeyValue.groupCount() == 2)
            {
                final String key = matcherKeyValue.group(1);
                final String value = matcherKeyValue.group(2);

                if (Pattern.compile("^-?\\d+$").matcher(value).matches())
                    map.put(key, Integer.valueOf(value));
                else if (Pattern.compile("^-?\\d+\\.(\\d+)?$").matcher(value).matches())
                    map.put(key, Float.valueOf(value));
                else if (Pattern.compile("[tT]rue|[Yy]es").matcher(value).matches())
                    map.put(key, Boolean.TRUE);
                else if (Pattern.compile("[Ff]alse|[Nn]o").matcher(value).matches())
                    map.put(key, Boolean.FALSE);
                else
                    map.put(key, value);
            }
        }

        configScanner.close();
    }

    public static Map<String, Object> fromFile(final String configPath)
    {
        final Map<String, Object> map = new HashMap<String, Object>();

        try
        {
            if ((new File(configPath)).exists())
                scanString(new String(Files.readAllBytes(Paths.get(configPath))), map);
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }

        return map;
    }

    public static Map<String, Object> fromString(final String configString)
    {
        final Map<String, Object> map = new HashMap<String, Object>();

        scanString(configString, map);

        return map;
    }

    public static Map<String, Object> filter(final Map<String, Object> map, final String key)
    {
        final Map<String, Object> filteredMap = new HashMap<String, Object>();

        final Pattern patternKey = Pattern.compile("^" + key + "\\.(.*)");

        for (final Map.Entry<String, Object> entry : map.entrySet())
        {
            final Matcher matcherKey = patternKey.matcher(entry.getKey());

            if (matcherKey.matches())
                filteredMap.put(matcherKey.group(1), entry.getValue());
        }

        return filteredMap;
    }
}
