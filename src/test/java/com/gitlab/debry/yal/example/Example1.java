package com.gitlab.debry.yal.example;

import com.gitlab.debry.yal.YAL;
import com.gitlab.debry.yal.input.level.E_Level;
import com.gitlab.debry.yal.input.level.User;
import com.gitlab.debry.yal.output.JConsole;
import com.gitlab.debry.yal.style.font.E_FontWeight;

public class Example1
{
    public static void main(final String[] args)
    {
        System.out.println("YAL - Example 1");
        final YAL logger = new YAL();

        final JConsole jconsole = (new JConsole("JConsole")).addLevel(E_Level.ALL);
        logger.addJConsole(jconsole);
        logger.INFO(0).Time().Str("Hello world").fontSize(20).fontWeight(E_FontWeight.BOLD).Int(10).format("==%d==").Endl();
        logger.INFO(0).Time().Str("lo world").bgRed().className().lineNo().format("line=%d").Endl();
        logger.INFO(1).Time().Str("los world").bgRed().className().lineNo().format("line=%d").Endl();
        logger.DEBUG(2).Time().Str("lo world").bgRed().className().lineNo().format("line=%d").Endl();

        final int[] data = new int[30];
        for (int i = 0; i < data.length; ++i)
            data[i] = i;

        logger.DEBUG(0).Time().Str("vector = ").NoSep().Vector(data).Endl();
        logger.DEBUG(1).Time().Str("part of vector = ").NoSep().Vector(data, 7, 15).Endl();
        logger.DEBUG(2).Time().Str("matrix = ").NoSep().Matrix(data, 6, 5).Endl();

        logger.WARN(0).Time().Str("this is a warning").strikeThrough().bgColor("orchid").Endl();
        logger.WARN(3).Time().stackAll().format("<<%s>>").underLine().Endl();
        logger.ERROR().Time().Str("This is an error").bgMagenta().italic().Endl();
        logger.FATAL().Time().Str("This is a unrecoverable error").fontSize(30).fgRed().fontFamily("monospace").Endl();
        logger.DEBUG(1).Time().Str("color").fgColor("darkslateblue").bold().bgGray().Endl();
        logger.DEBUG(2).Time().Str("color").Space(10).bgColor("brown").Int(34).Space(5).Real(5.633)
                .newLine().Str("test").lineNo().className()
                .Bool(false).format("Yep|Aiee").Str("== hello ==").Endl();

        User.setFormat(0, "Myformat");
        logger.USER(0).Time().Str("User message").fgBlack().bgPink().bold().Endl();

        logger.INFO(0).Str("test").Endl();
        logger.INFO(1).Str("test").Endl();
        logger.INFO(2).Str("test").Endl();
        logger.INFO(3).Str("test").Endl();
        logger.DEBUG(0).Str("test").Endl();
        logger.DEBUG(1).Str("test").Endl();
        logger.DEBUG(2).Str("test").Endl();
        logger.DEBUG(3).Str("test").Endl();
        logger.WARN(0).Str("test").Endl();
        logger.WARN(1).Str("test").Endl();
        logger.WARN(2).Str("test").Endl();
        logger.WARN(3).Str("test").Endl();
        logger.USER(0).Str("test").Endl();
        logger.USER(1).Str("test").Endl();
        logger.USER(2).Str("test").Endl();
        logger.USER(3).Str("test").Endl();
        logger.USER(4).Str("test").Endl();
        logger.INFO(0).Str("▬▬▬▬▬▬▬▬▬▬▬▷").fontSize(25).Endl();
        ((JConsole) jconsole).listAvailableFontFamilyName();
        ((JConsole) jconsole).listColor();
    }
}
